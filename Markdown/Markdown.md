__Essentiel du MARKDOWN__

Mise à jour le 2 Juillet 2019 par Pascal LUCAS

**Les paragraphes :**

--> séparation par des lignes vides

paragraphe N°1

paragraphe N°2

**Emphase (gras, italique)**

- Italique : _Texte en Italique_ (caractère underscore) ou *Texte en Italique* (caractère *)
- Gras     : __Texte en Gras__ (2 caractères underscore) ou **Texte en Gras**

**Les Titres**

2 Niveaux de titre, équivalents à h1 et h2 :

- Titre en h1 : mettre ======= sous le texte du titre
  ===========
- Titre en h2 : mettre ------- sous le texte du titre
  -----------

**Les Listes**

_Listes à puce :_

* Liste à puce ligne N°1
* Liste à puce ligne N°2
* Liste à puce ligne N°3

* Liste à puce ligne N°1
    * 2ème niveau Ligne N°1_1
    * 2ème niveau Ligne N°1_2
* Liste à puce ligne N°2
* Liste à puce ligne N°3

_Listes à puces numérotées :_

Il suffit de commencer la ligne par un numéro.

1. Liste à puce numérotée ligne N°1
2. Liste à puce numérotée ligne N°2
3. Liste à puce numérotée ligne N°3

avec des sous-niveaux :

1. Liste à puce numérotée ligne N°1
    1. 2ème niveau Ligne N°1_1
    2. 2ème niveau Ligne N°1_2
2. Liste à puce numérotée ligne N°2
3. Liste à puce numérotée ligne N°3


__Citations__

Faire précéder la citation par un chevron >

> Ceci est une citation

__Code source__

Identer le code source de 4 espaces.

Exemple de code source :

    def affichage_message(message):
        print(message)
        
    # Programme principal
    if __name__ == '__main__':
        doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)
        import sys 
        affichage_message()

Code en ligne : permet d'afficher du code dans un paragraphe

La fonction `print("hello")` permet d'afficher du texte

__Les liens__

Placer le texte du lien entre crochets []

[Site du NSI au lycée Louis Pasteur : ](http://legt-henin.fr/nsi.legt/)

__Les images__

Placer un point d'exclamation avant le texte pointant vers les images.

![Logo NSI au Lycée Pasteur](http://legt-henin.fr/nsi.legt/images/LogoNSI_R.jpg)

__Barre de séparation__

paragraphe N°1
1 ---------
paragraphe N°2
