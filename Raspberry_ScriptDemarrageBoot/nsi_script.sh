#!/bin/bash

### BEGIN INIT INFO
# Provides:          nsi_script
# Required-Start:    $all
# Required-Stop:    
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Script pour demarrer un script python au boot
# Description:       realise par Pascal LUCAS le 19 juin 2019
### END INIT INFO

# Apres cette ligne les commandes executees en fonction du parametre passe en ligne de commande

case "$1" in

    start)
        # Commandes executees avec le parametre start (celui lors du boot)		
        cd /home/pi
		screen -dmS pi python nsi_led_clignotante.py 
        ;;

    stop)
        # Commandes executees avec le parametre stop (celui lors de l'arret du systeme)
        ;;

    reload|restart)
        $0 stop
        $0 start
        ;;

    *)

        echo "Usage: $0 start|stop|restart|reload"

        exit 1

esac

exit 0
