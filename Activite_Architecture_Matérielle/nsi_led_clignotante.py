#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod:`nsi_led_clignotante` module
:date: 7 Juin 2019
:auteur: Pascal LUCAS

"""
# Bibliothèques utilisées

from gpiozero import LED
from time import sleep
import sys

# Port GPIO
led=LED(2)  # GPIO 2 utilisé pour le câblage de la LED

# Constante du programme principal
TIME_ON = 0.2
TIME_OFF = 0.5

# Programme principal
if __name__ == "__main__":
    # récupération des passées en  argument sur la ligne de commande
    print(len(sys.argv))
    print(sys.argv[0])
    if (len(sys.argv)>1):
         TIME_ON = float(sys.argv[1])
    if (len(sys.argv)>2):
         TIME_OFF = float(sys.argv[2])     
    print("NSI, commande d'une LED ", TIME_ON, "s pendant ", TIME_ON+TIME_OFF, " secondes")
    while True:
        led.on()
        sleep(TIME_ON)
        led.off()
        sleep(TIME_OFF)

