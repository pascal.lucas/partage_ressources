## Bloc 3 – Activités Systèmes ##

### Objectif : ###

Mettre en oeuvre les commandes de base du shell sous LINUX afin d'exécuter un script PYTHON 
commandant de façon cyclique un voyant câblé sur le port GPIO d'une carte RASPBERRY PI ZERO.
Programmer un automatisme simple de gestion d'éclairage en Python en captant l'état d'un interrupteur (ou bouton poussoir)
et en commandant un voyant de type LED.

### Éléments du programme en NSI : ###

Système d'exploitation :
 - Utiliser les commandes de base en ligne de commande
 - Gérer les droits et permissions d'accès aux fichiers

Périphériques d'entrée et de sortie :
- Identifier le rôle des capteurs et des actionneurs

### Sujet abordé : ###

OS LINUX, Nano-ordinateur, périphériques E/S


### Pré-requis : ###

Apports de connaissances concernant les OS et les commandes de base sous LINUX. 

### Préparation : ###

Carte RASPBERRY PI ZERO dont la carte SD aura été configurée avec les bibliothèques Python, les scripts permettant la commande de la LED.
Cette carte pourra être raccordée à un port USB d'un PC sous Windows7 ou 10, ou connecté à un point d'accès WIFI dédié au TP.
Le fichier PDF [Préparation carte RASPBERRY PI pour la NSI](https://gitlab-fil.univ-lille.fr/pascal.lucas/partage_ressources/blob/master/Activite_Architecture_Mat%C3%A9rielle/NSI_RaspberryPI_0.pdf) 
décrit la configuration nécessaire pour les TP en NSI avec cette carte.

## Éléments de cours : ###
- LINUX : shell et commandes de base 
- Capteurs, actionneurs (entrée/sortie d'une carte micro-programmée)

### Séance pratique : ###

- [Activité SHELL N°1 :](Activite_Architecture_Matérielle/NSI_Linux_Activite_Shell_1.pdf) [(au format .odt)](Activite_Architecture_Matérielle/NSI_Linux_Activite_Shell_1.odt)
- [Activité SHELL N°2 :](Activite_Architecture_Matérielle/NSI_Linux_Activite_Shell_2.pdf) [(au format .odt)](Activite_Architecture_Matérielle/NSI_Linux_Activite_Shell_2.odt)
- [Activité Capteurs et actionneurs :](Activite_Architecture_Matérielle/NSI_CapteursActionneurs.pdf) [(au format .odt)](Activite_Architecture_Matérielle/NSI_CapteursActionneurs.odt)


[Dossier reprenant les procedures d'installation, driver, etc...](https://gitlab-fil.univ-lille.fr/pascal.lucas/partage_ressources/tree/master/Activite_Architecture_Mat%C3%A9rielle)

## QCM E3C

q1 : lors de l'ouverture d'un terminal sous LINUX le message suivant apparait : pascal@localhost:~$
 - pascal est le nom de l'utilisateur de la session sur laquelle le terminal a été ouverte
 - pascal est le nom de l'ordianteur sur lequel le terminal a été ouvert
 - pascal est le dossier dans lequel des données pourront être consultées ou modifiées sur ce terminal
 - pascal est le nom du fichier ouvert par le terminal
 

q2 : La commande ls -la produit les informations suivantes :
-rw-r--r-- 1 pascal commun     845 juin  12 09:12 nsi_led_clignotante.py
- tous les utilisateurs peuvent lire ce script python 
- tous les utilisateurs peuvent modifier ce script python
- tous les utilisateurs peuvent effacer ce script python
- tous les utilisateurs peuvent modifier le nom de ce fichier


q3 : Un interrupteur est un capteur fournissant au micro-processeur une information de type :
- binaire sur un bit
- binaire sur un octet
- binaire sur un mot de 16 bits
- binaire sur un mot de 32 bits

